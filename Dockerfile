# Dockerfile 
FROM openjdk:21
RUN mkdir /usr/app/
COPY target/Deepavali_App.jar /usr/app
WORKDIR /usr/app/ 
ENTRYPOINT [ "java","-jar","Deepavali_App.jar" ]